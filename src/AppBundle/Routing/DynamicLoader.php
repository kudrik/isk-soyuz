<?php
namespace AppBundle\Routing;

use AppBundle\Entity\Repository\SectionRepository;
use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Section;

class DynamicLoader extends Loader
{
    private $loaded = false;

    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function load($resource, $type = null)
    {
        if (true === $this->loaded) {
            throw new \RuntimeException('Do not add the "extra" loader twice');
        }

        $routes = new RouteCollection();

        $SectionRepositary = $this->em->getRepository(Section::class);

        foreach ($SectionRepositary->findAll() as $page) {

            if ($page->getUrl()=='root') {
                continue;
            }

            $urls = array();

            foreach ($page->getAllParents() as $parent) {

                array_unshift($urls, trim($parent->getUrl()));
            }

            $urls[] = trim($page->getUrl());

            $url = mb_strtolower('/'.implode('/',$urls));

            //exclude main page
            if ($url=='/index') {
                $url='/';
            }

            $route = new Route($url, array(
                '_controller' => 'AppBundle:Page:index',
                'pageId' => $page->getId(),
            ));

            $routes->add('section'.$page->getId(), $route);
        }

        $this->loaded = true;

        return $routes;
    }

    public function supports($resource, $type = null)
    {
        return 'extra' === $type;
    }
}