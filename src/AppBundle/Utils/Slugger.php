<?php
namespace AppBundle\Utils;

use Transliterator;

class Slugger
{
    public static function slugify($string)
    {
        $string = strip_tags($string);

        $rule = 'Cyrillic-Latin';
        $transliterator = Transliterator::create($rule);
        $string = $transliterator->transliterate($string);

        return preg_replace(
            '/[^a-z0-9]/', '_', mb_strtolower(trim($string))
        );
    }
}