<?php
namespace AppBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use AppBundle\Entity\User;


class UserListner
{
    private $password_encoder;

    public function __construct(UserPasswordEncoder $password_encoder, \Twig_Environment $twig, \Swift_Mailer $mailer)
    {
        $this->password_encoder = $password_encoder;
        $this->twig = $twig;
        $this->mailer = $mailer;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $this->hashPassword($args);
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $this->hashPassword($args);
    }

    protected function hashPassword(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        // only act on some "User" entity
        if ($entity instanceof User) {

            if ($entity->getPlainPassword()) {

                $entity->setPassword(
                    $this->password_encoder->encodePassword($entity, $entity->getPlainPassword())
                );
            }
        }
    }
}