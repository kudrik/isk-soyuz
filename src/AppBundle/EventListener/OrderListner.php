<?php
namespace AppBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use AppBundle\Entity\Order;

class OrderListner
{
    protected $twig;
    protected $mailer;
    protected $mailfrom;
    protected $mailToList;
    protected $siteName;

    public function __construct(\Twig_Environment $twig, \Swift_Mailer $mailer, String $mailFrom, String $mailTo, String $siteName)
    {
        $this->twig = $twig;
        $this->mailer = $mailer;
        $this->mailFrom = $mailFrom;
        $this->mailToList = explode(',',$mailTo);
        $this->siteName = $siteName;
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $order = $args->getEntity();

        if ($order instanceof Order) {

            $message = \Swift_Message::newInstance()
                ->setSubject('Заказ с сайта '.$this->siteName)
                ->setFrom($this->mailFrom)
                ->setTo($this->mailToList)
                ->setBody(
                    $this->twig->render(
                        'order/email.html.twig',
                        array('Order' => $order)
                    ),
                    'text/html'
                )
                /*
                 * If you also want to include a plaintext version of the message
                ->addPart(
                    $this->renderView(
                        'Emails/registration.txt.twig',
                        array('name' => $name)
                    ),
                    'text/plain'
                )
                */
            ;

            $this->mailer->send($message);
        }
    }
}