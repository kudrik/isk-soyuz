<?php
namespace AppBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Filesystem\Filesystem;
use AppBundle\Entity\Section;
use AppBundle\Entity\Menu;

class ClearCacheService
{
    private $cache_dir;

    public function __construct(string $cache_dir)
    {
        $this->cache_dir = $cache_dir;
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $this->clear($args);
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->clear($args);
    }

    public function postRemove(LifecycleEventArgs $args)
    {
        $this->clear($args);
    }

    protected function clear(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        // only act on some "User" entity
        if ($entity instanceof Section || $entity instanceof Menu) {

            $fs = new Filesystem();
            $fs->remove(
                array($this->cache_dir)
            );
        }
    }
}