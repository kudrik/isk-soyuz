<?php
namespace AppBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use AppBundle\Utils\Slugger;
use AppBundle\Entity\Section;

class SluggerListner
{
    public function prePersist(LifecycleEventArgs $args)
    {
        $this->process($args);
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $this->process($args);
    }

    private function process(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        // only act on some "Section" entity
        if ($entity instanceof Section) {

            if (!($url = $entity->getUrl())) {

                $url = $entity->getName();
            }

            $entity->setUrl(Slugger::slugify($url));

            $entity->getUrl();

            return;
        }

        //$entityManager = $args->getEntityManager();
        // ... do something with the Product

    }
}