<?php

namespace AppBundle\Service;

use AppBundle\Entity\Section;
use Doctrine\Bundle\DoctrineBundle\Registry;

class District
{
    protected $doctrine;

    public function __construct(Registry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function getList()
    {
        return $this->doctrine->getRepository(Section::class)->findAllDistricts();
    }
}