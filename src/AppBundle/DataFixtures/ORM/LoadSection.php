<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;
use AppBundle\Entity\Section;
use AppBundle\Entity\Menu;

class LoadSection extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    const sections = [
        ['name'=>'Главная',         'url'=>'index',     'menu'=>true],
        ['name'=>'ЖК Акварельный',          'url'=>'akvarelny',     'menu'=>false,  'type'=>1,  'announce'=>'Кировский район, ул. Николая Сотникова'],
        ['name'=>'Жилой дом «Олимпийский»', 'url'=>'olimp',         'menu'=>false,  'type'=>1,  'announce'=>'Ленинский район, ул. Колхидская, 6 стр.'],
        ['name'=>'Дом на ул. Одоевского',   'url'=>'odoevskogo',    'menu'=>false,  'type'=>1,  'announce'=>'Первомайский район, ул. Одоевского, стр. 9/1'],
        ['name'=>'О компании',      'url'=>'about',     'menu'=>true,],
        ['name'=>'Контакты',        'url'=>'contacts',  'menu'=>true,],
        ['name'=>'Соглашение об обработке персональных данных',        'url'=>'privacy'],
    ];

    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        //add root
        $rootSection = new Section();
        $rootSection->setName('Root');
        $rootSection->setUrl('root');
        $manager->persist($rootSection);
        $manager->flush();

        //add others
        $this->addSections($manager, $rootSection, self::sections);
    }

    private function addSections(ObjectManager $manager, $parent, $sections )
    {
        foreach ($sections as $position=>$row) {

            $section = new Section();
            $section->setName($row['name']);
            $section->setUrl($row['url']);
            $section->setPosition($position*10);
            if (isset($row['announce'])) {
                $section->setAnnounce($row['announce']);
            }
            if (isset($row['type'])) {
                $section->setType($row['type']);
            }
            $section->setParent($parent);
            $manager->persist($section);
            $manager->flush();

            //add to menu
            if (isset($row['menu']) && $row['menu']) {
                $menu = new Menu();
                $menu->setSection($section);
                $menu->setPosition($section->getPosition());
                $manager->persist($menu);
                $manager->flush();
            }

            if (isset($row['childrens'])) {
                $this->addSections($manager, $section, $row['childrens']);
            }
        }

        $fs = new Filesystem();
        $fs->remove($this->container->getParameter('kernel.cache_dir'));
    }

    public function getOrder()
    {
        return 1;
    }
}