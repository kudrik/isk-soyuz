<?php

namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Builder implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');

        $em = $this->container->get('doctrine')->getManager();

        //advansed items menu (for example in districts)
        if (isset($options['advancedItems']) && $options['advancedItems']) {
            foreach ($options['advancedItems'] as $item) {
                $menu->addChild($item['name'], array('uri' => $item['uri']));
            }
        }

        foreach ($em->getRepository('AppBundle:Menu')->findBy(array(),array('position' => 'ASC')) as $item) {

            if ($item->getUrl()) {
                $menu->addChild($item->getFullName(), array('uri' => $item->getUrl()));
            } else {
                $menu->addChild($item->getFullName(), array('route' => 'section'.$item->getSection()->getId()));
            }
        }

        return $menu;
    }
}