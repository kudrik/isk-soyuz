<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class FlatAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('plan', 'entity', array(
            'class' => 'AppBundle\Entity\Plan',
            'choice_label' => 'id',
        ));
        $formMapper->add('floor', 'integer');
        $formMapper->add('price', 'integer');
        $formMapper->add('hot', 'integer',  array('required' => false,));
        //$formMapper->add('s_kitchen', 'number', array('scale' => 2, ));
        //$formMapper->add('s_loggia', 'number', array('scale' => 2, ));
        //$formMapper->add('description', 'text', array('required' => false,));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        /*
        $datagridMapper->add('plan', null, array(), 'entity', array(
            'class'    => 'AppBundle\Entity\Plan',
            'choice_label' => 'name', // In Symfony2: 'property' => 'name'
        ));
        */
        $datagridMapper->add('floor');
        $datagridMapper->add('price');
        $datagridMapper->add('hot');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('plan.building.name');
        $listMapper->addIdentifier('plan.rooms');
        $listMapper->addIdentifier('plan.s_total');
        $listMapper->addIdentifier('floor');
        $listMapper->addIdentifier('price');
        $listMapper->addIdentifier('hot');

    }
}