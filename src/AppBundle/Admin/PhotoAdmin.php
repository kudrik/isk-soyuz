<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use AppBundle\Entity\Section;

class PhotoAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('section', 'entity', array(
            'class' => Section::class,
            'choice_label' => 'name',
        ));
        $formMapper->add('name', 'text', array('required' => false,));

        $subject = $this->getSubject();

        if ($subject->getId()>0) {

            $formMapper->add('image', 'file', array('required' => false, 'help'=>$subject->getImgPreviewTag()));

        } else {

            $formMapper->add('image', 'file');
        }

        $formMapper->add('position', 'integer', array('required' => false,));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('section', null, array(), 'entity', array(
            'class'    => Section::class,
            'choice_label' => 'name',
        ));
        $datagridMapper->add('name');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('section.name');
        $listMapper->addIdentifier('name');
        $listMapper->addIdentifier('imgPreviewTag','html');
    }

    public function preUpdate($subject)
    {
        if ($subject->getImage()) {
            $subject->refreshUpdated();
        }
    }
}