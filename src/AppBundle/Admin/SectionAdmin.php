<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class SectionAdmin extends AbstractAdmin
{


    protected function configureFormFields(FormMapper $formMapper)
    {
        $subject = $this->getSubject();

        $formMapper
            ->with('Content', array('class' => 'col-md-6'))
                ->add('parent', 'entity', array(
                    'class' => 'AppBundle\Entity\Section',
                    'choice_label' => 'name',
                ))
                ->add('name', 'text')
                ->add('url', 'text', array('required' => false,))
            ->end()
            ->with('Seo', array('class' => 'col-md-6'))
                ->add('meta_title', 'text', array('required' => false,))
                ->add('meta_key', 'text', array('required' => false,))
                ->add('meta_desc', 'text', array('required' => false,))
            ->end()
            ->with('Description', array('class' => 'col-md-12'))
                ->add('description', 'textarea', array('required' => false,'attr'=>array('class'=>'mceAdvanced')))
            ->end()
            ->with('Advanced', array('class' => 'col-md-4'))
                ->add('show_tit', 'checkbox', array('required' => false,))
                ->add('show_sub', 'checkbox', array('required' => false,))
                ->add('position', 'integer', array('required' => false,))
                ->add('image', 'file', array('required' => false, 'help'=>$subject->getImgPreviewTag()))
            ->end()
            ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        /*
        $datagridMapper->add('section', null, array(), 'entity', array(
            'class'    => 'AppBundle\Entity\Section',
            'choice_label' => 'name',
        ));
        */
        $datagridMapper->add('name');

    }

    protected function configureListFields(ListMapper $listMapper)
    {
        //$listMapper->addIdentifier('section.name');
        $listMapper->addIdentifier('name');
        $listMapper->addIdentifier('url');

    }

    //triger update for file upload
    public function preUpdate($subject)
    {
        if ($subject->getImage()) {
            $subject->refreshUpdated();
        }
    }
}