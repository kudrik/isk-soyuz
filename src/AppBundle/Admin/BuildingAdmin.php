<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;


class BuildingAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $subject = $this->getSubject();

        $formMapper->add('section', 'entity', array(
            'class' => 'AppBundle\Entity\Section',
            'choice_label' => 'name',
        ));
        $formMapper->add('name', 'text');
        $formMapper->add('address', 'text', array('required' => false,));
        $formMapper->add('announce', 'text', array('required' => false,));
        $formMapper->add('compl_time', 'text', array('required' => false,));
        $formMapper->add('material', 'text', array('required' => false,));
        $formMapper->add('floors', 'text', array('required' => false,));
        $formMapper->add('description', 'text', array('required' => false,));
        $formMapper->add('image', 'file', array('required' => false));
        $formMapper->add('position', 'integer', array('required' => false,));
        $formMapper->add('image', 'file', array('required' => false, 'help'=>$subject->getImgPreviewTag()));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('section', null, array(), 'entity', array(
            'class'    => 'AppBundle\Entity\Section',
            'choice_label' => 'name',
        ));
        $datagridMapper->add('name');
        //$datagridMapper->add('address');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('section.name');
        $listMapper->addIdentifier('name');
        $listMapper->addIdentifier('address');

    }

    //triger update for file upload
    public function preUpdate($subject)
    {
        if ($subject->getImage()) {
            $subject->refreshUpdated();
        }
    }
}