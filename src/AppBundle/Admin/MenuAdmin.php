<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class MenuAdmin extends AbstractAdmin
{
    protected $datagridValues = array(
        '_sort_order' => 'ASC',
        '_sort_by' => 'position',
    );

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('section', 'entity', array(
            'class' => 'AppBundle\Entity\Section',
            'choice_label' => 'name',
            'required' => false,
        ));
        $formMapper->add('name', 'text', array('required' => false,));
        $formMapper->add('url', 'text', array('required' => false,));
        $formMapper->add('position', 'integer', array('required' => false,));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('section', null, array(), 'entity', array(
            'class'    => 'AppBundle\Entity\Section',
            'choice_label' => 'name',
        ));
        $datagridMapper->add('name');
        $datagridMapper->add('url');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('section.name');
        $listMapper->addIdentifier('name');
        $listMapper->addIdentifier('url');

    }
}