<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class OrderAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper->add('theme', 'text');
        $formMapper->add('contact_name', 'text');
        $formMapper->add('contact_phone', 'text');
        $formMapper->add('description', 'textarea');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('theme');
        $datagridMapper->add('contact_name');
        $datagridMapper->add('contact_phone');

    }

    protected function configureListFields(ListMapper $listMapper)
    {
        //$listMapper->addIdentifier('plan.id');
        $listMapper->addIdentifier('created');
        $listMapper->addIdentifier('theme');
        $listMapper->addIdentifier('contact_name');
        $listMapper->addIdentifier('contact_phone');


    }
    
    public function configureShowFields(ShowMapper $showMapper)
    {
    	$showMapper
    	->tab('General') // the tab call is optional
    	->with('Addresses', array(
    			'class'       => 'col-md-8',
    			'box_class'   => 'box box-solid box-danger',
    			'description' => 'Lorem ipsum',
    	))
    	->add('theme')
    	->add('contact_name')
    	->add('contact_phone')
    	// ...
    	->end()
    	->end()
    	;
    }
}