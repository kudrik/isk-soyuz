<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class PlanAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $subject = $this->getSubject();

        $formMapper->add('building', 'entity', array(
            'class' => 'AppBundle\Entity\Building',
            'choice_label' => 'name',
        ));
        $formMapper->add('rooms', 'integer');
        $formMapper->add('s_total', 'number', array('scale' => 2, ));
        $formMapper->add('s_living', 'number', array('scale' => 2, ));
        $formMapper->add('s_kitchen', 'number', array('scale' => 2, ));
        $formMapper->add('loggia', 'checkbox', array('required' => false,));
        $formMapper->add('s_loggia', 'number', array('scale' => 2, ));
        $formMapper->add('description', 'text', array('required' => false,));
        $formMapper->add('image', 'file', array('required' => false, 'help'=>$subject->getImgPreviewTag()));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('building', null, array(), 'entity', array(
            'class'    => 'AppBundle\Entity\Building',
            'choice_label' => 'name', // In Symfony2: 'property' => 'name'
        ));
        $datagridMapper->add('rooms');
        $datagridMapper->add('s_total');
        $datagridMapper->add('s_living');
        $datagridMapper->add('s_kitchen');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('building.name');
        $listMapper->addIdentifier('imgPreviewTag','html');
        $listMapper->addIdentifier('rooms');
        $listMapper->addIdentifier('s_total');
        $listMapper->addIdentifier('s_living');
        $listMapper->addIdentifier('s_kitchen');
        $listMapper->addIdentifier('s_loggia');
    }

    //triger update for file upload
    public function preUpdate($subject)
    {
        if ($subject->getImage()) {
            $subject->refreshUpdated();
        }
    }

}