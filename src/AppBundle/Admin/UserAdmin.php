<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class UserAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('email', 'email');

        $subject = $this->getSubject();

        if ($subject->getId()>0) {
            $formMapper->add('plainPassword', 'text', array('required' => false));
        } else {
            $formMapper->add('plainPassword', 'text');
        }

        $formMapper->add('role', 'integer');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('email');

       
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('email');
        $listMapper->addIdentifier('roleName');
    }

    //triger update for generate password
    public function preUpdate($user)
    {
        if ($user->getPlainPassword()) {
            $user->setUpdated();
        }
    }
}