<?php

namespace AppBundle\Entity;

/**
 * Flat
 */
class Flat
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $floor;

    /**
     * @var integer
     */
    private $price;

    /**
     * @var string
     */
    private $entrances;

    /**
     * @var integer
     */
    private $hot;

    /**
     * @var \AppBundle\Entity\Plan
     */
    private $plan;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set floor
     *
     * @param integer $floor
     *
     * @return Flat
     */
    public function setFloor($floor)
    {
        $this->floor = $floor;

        return $this;
    }

    /**
     * Get floor
     *
     * @return integer
     */
    public function getFloor()
    {
        return $this->floor;
    }

    /**
     * Set price
     *
     * @param integer $price
     *
     * @return Flat
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return integer
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set entrances
     *
     * @param string $entrances
     *
     * @return Flat
     */
    public function setEntrances($entrances)
    {
        $this->entrances = $entrances;

        return $this;
    }

    /**
     * Get entrances
     *
     * @return string
     */
    public function getEntrances()
    {
        return $this->entrances;
    }

    /**
     * Set hot
     *
     * @param integer $hot
     *
     * @return Flat
     */
    public function setHot($hot)
    {
        $this->hot = $hot;

        return $this;
    }

    /**
     * Get hot
     *
     * @return integer
     */
    public function getHot()
    {
        return $this->hot;
    }

    /**
     * Set plan
     *
     * @param \AppBundle\Entity\Plan $plan
     *
     * @return Flat
     */
    public function setPlan(\AppBundle\Entity\Plan $plan = null)
    {
        $this->plan = $plan;

        return $this;
    }

    /**
     * Get plan
     *
     * @return \AppBundle\Entity\Plan
     */
    public function getPlan()
    {
        return $this->plan;
    }


    /* pseudo field */

    public function getDescription()
    {
        $description = $this->getPlan()->getBuilding()->getName().', '.$this->getPlan()->getBuilding()->getAddress().PHP_EOL;
        $description.= $this->getPlan()->getName().', пл: '. $this->getPlan()->getSTotal().'/'.$this->getPlan()->getSLiving().'/'.$this->getPlan()->getSKitchen().PHP_EOL;
        $description.= 'Этаж: ' . $this->getFloor() . PHP_EOL;
        $description.= 'Цена: '.number_format($this->getPrice(), 0,'.',' ').' руб.';

        return $description;
    }
}
