<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Traits\ImageableTrait;

/**
 * Plan
 */
class Plan
{
    use ImageableTrait;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $rooms;

    /**
     * @var string
     */
    private $s_total;

    /**
     * @var string
     */
    private $s_living;

    /**
     * @var string
     */
    private $s_kitchen;

    /**
     * @var string
     */
    private $s_loggia;

    /**
     * @var boolean
     */
    private $loggia;

    /**
     * @var string
     */
    private $description;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $flats;

    /**
     * @var \AppBundle\Entity\Building
     */
    private $building;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->flats = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rooms
     *
     * @param integer $rooms
     *
     * @return Plan
     */
    public function setRooms($rooms)
    {
        $this->rooms = $rooms;

        return $this;
    }

    /**
     * Get rooms
     *
     * @return integer
     */
    public function getRooms()
    {
        return $this->rooms;
    }

    /**
     * Set sTotal
     *
     * @param string $sTotal
     *
     * @return Plan
     */
    public function setSTotal($sTotal)
    {
        $this->s_total = $sTotal;

        return $this;
    }

    /**
     * Get sTotal
     *
     * @return string
     */
    public function getSTotal()
    {
        return $this->s_total;
    }

    /**
     * Set sLiving
     *
     * @param string $sLiving
     *
     * @return Plan
     */
    public function setSLiving($sLiving)
    {
        $this->s_living = $sLiving;

        return $this;
    }

    /**
     * Get sLiving
     *
     * @return string
     */
    public function getSLiving()
    {
        return $this->s_living;
    }

    /**
     * Set sKitchen
     *
     * @param string $sKitchen
     *
     * @return Plan
     */
    public function setSKitchen($sKitchen)
    {
        $this->s_kitchen = $sKitchen;

        return $this;
    }

    /**
     * Get sKitchen
     *
     * @return string
     */
    public function getSKitchen()
    {
        return $this->s_kitchen;
    }

    /**
     * Set sLoggia
     *
     * @param string $sLoggia
     *
     * @return Plan
     */
    public function setSLoggia($sLoggia)
    {
        $this->s_loggia = $sLoggia;

        return $this;
    }

    /**
     * Get sLoggia
     *
     * @return string
     */
    public function getSLoggia()
    {
        return $this->s_loggia;
    }

    /**
     * Set loggia
     *
     * @param boolean $loggia
     *
     * @return Plan
     */
    public function setLoggia($loggia)
    {
        $this->loggia = $loggia;

        return $this;
    }

    /**
     * Get loggia
     *
     * @return boolean
     */
    public function getLoggia()
    {
        return $this->loggia;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Plan
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add flat
     *
     * @param \AppBundle\Entity\Flat $flat
     *
     * @return Plan
     */
    public function addFlat(\AppBundle\Entity\Flat $flat)
    {
        $this->flats[] = $flat;

        return $this;
    }

    /**
     * Remove flat
     *
     * @param \AppBundle\Entity\Flat $flat
     */
    public function removeFlat(\AppBundle\Entity\Flat $flat)
    {
        $this->flats->removeElement($flat);
    }

    /**
     * Get flats
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFlats()
    {
        return $this->flats;
    }

    /**
     * Set building
     *
     * @param \AppBundle\Entity\Building $building
     *
     * @return Plan
     */
    public function setBuilding(\AppBundle\Entity\Building $building = null)
    {
        $this->building = $building;

        return $this;
    }

    /**
     * Get building
     *
     * @return \AppBundle\Entity\Building
     */
    public function getBuilding()
    {
        return $this->building;
    }
    /**
     * @var \DateTime
     */
    private $updated;


    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return Plan
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    public function refreshUpdated()
    {
        $this->setUpdated(new \DateTime());
    }

    public function getName() {

        if ($this->getRooms()==0) { return 'Студия'; }
        if ($this->getRooms()==99) { return 'Коммерческое помещение'; }
        return $this->getRooms().'-комнатная квартира';
    }
}
