<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Traits\ImageableTrait;

/**
 * Building
 */
class Building
{
    use ImageableTrait;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $position;

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $announce;

    /**
     * @var string
     */
    private $complTime;

    /**
     * @var string
     */
    private $imgCoords;

    /**
     * @var string
     */
    private $material;

    /**
     * @var string
     */
    private $floors;

    /**
     * @var string
     */
    private $description;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $plans;

    /**
     * @var \AppBundle\Entity\Section
     */
    private $section;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->plans = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Building
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return Building
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Building
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set announce
     *
     * @param string $announce
     *
     * @return Building
     */
    public function setAnnounce($announce)
    {
        $this->announce = $announce;

        return $this;
    }

    /**
     * Get announce
     *
     * @return string
     */
    public function getAnnounce()
    {
        return $this->announce;
    }

    /**
     * Set complTime
     *
     * @param string $complTime
     *
     * @return Building
     */
    public function setComplTime($complTime)
    {
        $this->complTime = $complTime;

        return $this;
    }

    /**
     * Get complTime
     *
     * @return string
     */
    public function getComplTime()
    {
        return $this->complTime;
    }

    /**
     * Set imgCoords
     *
     * @param string $imgCoords
     *
     * @return Building
     */
    public function setImgCoords($imgCoords)
    {
        $this->imgCoords = $imgCoords;

        return $this;
    }

    /**
     * Get imgCoords
     *
     * @return string
     */
    public function getImgCoords()
    {
        return $this->imgCoords;
    }

    /**
     * Set material
     *
     * @param string $material
     *
     * @return Building
     */
    public function setMaterial($material)
    {
        $this->material = $material;

        return $this;
    }

    /**
     * Get material
     *
     * @return string
     */
    public function getMaterial()
    {
        return $this->material;
    }

    /**
     * Set floors
     *
     * @param string $floors
     *
     * @return Building
     */
    public function setFloors($floors)
    {
        $this->floors = $floors;

        return $this;
    }

    /**
     * Get floors
     *
     * @return string
     */
    public function getFloors()
    {
        return $this->floors;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Building
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return Building
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Add plan
     *
     * @param \AppBundle\Entity\Plan $plan
     *
     * @return Building
     */
    public function addPlan(\AppBundle\Entity\Plan $plan)
    {
        $this->plans[] = $plan;

        return $this;
    }

    /**
     * Remove plan
     *
     * @param \AppBundle\Entity\Plan $plan
     */
    public function removePlan(\AppBundle\Entity\Plan $plan)
    {
        $this->plans->removeElement($plan);
    }

    /**
     * Get plans
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlans()
    {
        return $this->plans;
    }

    /**
     * Set section
     *
     * @param \AppBundle\Entity\Section $section
     *
     * @return Building
     */
    public function setSection(\AppBundle\Entity\Section $section = null)
    {
        $this->section = $section;

        return $this;
    }

    /**
     * Get section
     *
     * @return \AppBundle\Entity\Section
     */
    public function getSection()
    {
        return $this->section;
    }

    public function refreshUpdated()
    {
        $this->setUpdated(new \DateTime());
    }

    /**
     * @return array
     */
    public function getFlatsInGroup()
    {
        $flats = array();

        foreach ($this->getPlans() as $plan) {

            foreach ($plan->getFlats() as $flat) {

                $key = $plan->getId().'_'.$flat->getPrice();

                if (!isset($flats[$key])) {
                    $flats[$key] = array(
                        'id'=>$flat->getId(),
                        'plan'=>$plan,
                        'floors'=>array(),
                        'price'=>$flat->getPrice(),
                        'hot'=>0,
                    );
                }

                if ($flat->getHot()) {
                    $flats[$key]['hot'] = $flat->getHot();
                }

                $flats[$key]['floors'][$flat->getFloor()] = $flat->getFloor();
            }
        }

        return $flats;
    }
}
