<?php

namespace AppBundle\Entity\Traits;

trait ImageableTrait
{
    /**
     * Unmapped property to handle image uploads
     */
    private $image;

    public function getImagePath()
    {
        return '/images/'.strtolower(basename(str_replace('\\','/',get_class($this)))).'/';
    }

    public function getImageSizes()
    {
        return array(
            'Orig' => array(null, null, null),
            'L' => array(800, 600, 90),
            'M' => array(400, 300, 90),
            'S' => array(200, 100, 90)
        );
    }

    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function getImageOrigName()
    {
        return $this->getImagePath().$this->getId().'.jpg';
    }

    public function getImageLName()
    {
        return $this->getImagePath().$this->getId().'l.jpg';
    }

    public function getImageMName()
    {
        return $this->getImagePath().$this->getId().'m.jpg';
    }

    public function getImageSName()
    {
        return $this->getImagePath().$this->getId().'s.jpg';
    }

    public function getImgPreviewTag()
    {
        if ($this->getId()>0) {
            return '<img src="' . $this->getImageSName() . $this->getImageNameCachePostfix() . '" alt="" style="max-width:100px;" class="admin-preview">';
        }

        return '';
    }

    public function getImageNameCachePostfix()
    {
        if ($this->getUpdated()) {
            return '?c='.$this->getUpdated()->format('Y-m-d H:i:s');
        }

        return '';
    }
}