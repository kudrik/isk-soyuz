<?php

namespace AppBundle\Entity;

/**
 * Order
 */
class Order
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var string
     */
    private $contact_name;

    /**
     * @var string
     */
    private $contact_phone;

    /**
     * @var string
     */
    private $theme;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $userinfo;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->created = new \DateTime();
    }
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Order
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set contactName
     *
     * @param string $contactName
     *
     * @return Order
     */
    public function setContactName($contactName)
    {
        $this->contact_name = $contactName;

        return $this;
    }

    /**
     * Get contactName
     *
     * @return string
     */
    public function getContactName()
    {
        return $this->contact_name;
    }

    /**
     * Set contactPhone
     *
     * @param string $contactPhone
     *
     * @return Order
     */
    public function setContactPhone($contactPhone)
    {
        $this->contact_phone = $contactPhone;

        return $this;
    }

    /**
     * Get contactPhone
     *
     * @return string
     */
    public function getContactPhone()
    {
        return $this->contact_phone;
    }

    /**
     * Set theme
     *
     * @param string $theme
     *
     * @return Order
     */
    public function setTheme($theme)
    {
        $this->theme = $theme;

        return $this;
    }

    /**
     * Get theme
     *
     * @return string
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Order
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set userinfo
     *
     * @param string $userinfo
     *
     * @return Order
     */
    public function setUserinfo($userinfo)
    {
        $this->userinfo = $userinfo;

        return $this;
    }

    /**
     * Get userinfo
     *
     * @return string
     */
    public function getUserinfo()
    {
        return $this->userinfo;
    }

    /* pseudo fields */

    private $flatid;

    public function setFlatid($val)
    {
        $this->flatid = $val;
        return $this;
    }

    public function getFlatid()
    {
        return $this->flatid;
    }

    private $sameflats;

    public function getSameFlats()
    {
        return $this->sameflats;
    }

    public function setSameFlats($val)
    {
        $this->sameflats = $val;
        return $this;
    }
}

