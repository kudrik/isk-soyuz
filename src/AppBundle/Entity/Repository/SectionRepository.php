<?php

namespace AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class SectionRepository extends EntityRepository
{
    public function findAllDistricts()
    {
        return $this->createQueryBuilder('s')
            ->where('s.type = 1')
            ->getQuery()
            ->getResult();
    }
}