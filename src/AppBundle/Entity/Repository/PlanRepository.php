<?php

namespace AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class PlanRepository extends EntityRepository
{
    public function findByAttributes($attributes)
    {
        $qb = $this->createQueryBuilder('p');

        foreach ($attributes as $k=>$val) {
            $qb->andWhere('p.'.$k.' = :'.$k)->setParameter($k, $val);
        }

        return $qb->getQuery()->getOneOrNullResult();
    }
}