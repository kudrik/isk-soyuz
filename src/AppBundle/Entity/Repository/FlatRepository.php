<?php

namespace AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class FlatRepository extends EntityRepository
{
    public function removeAll()
    {
        foreach ($this->findAll() as $row) {

            $this->getEntityManager()->remove($row);
            $this->getEntityManager()->flush();
        }
    }
}