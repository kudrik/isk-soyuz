<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Traits\ImageableTrait;

/**
 * Section
 */
class Section
{
    use ImageableTrait;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $url;

    /**
     * @var integer
     */
    private $position = 0;

    /**
     * @var string
     */
    private $announce;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $meta_title;

    /**
     * @var string
     */
    private $meta_desc;

    /**
     * @var string
     */
    private $meta_key;

    /**
     * @var boolean
     */
    private $show_tit = true;

    /**
     * @var boolean
     */
    private $show_sub = true;

    /**
     * @var \AppBundle\Entity\Menu
     */
    private $menu;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $buildings;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $children;

    /**
     * @var \AppBundle\Entity\Section
     */
    private $parent;


    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * @var integer
     */
    private $type = 0;

    /**
     * @var string
     */
    private $map_coords;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->buildings = new \Doctrine\Common\Collections\ArrayCollection();
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Section
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Section
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return Section
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set announce
     *
     * @param string $announce
     *
     * @return Section
     */
    public function setAnnounce($announce)
    {
        $this->announce = $announce;

        return $this;
    }

    /**
     * Get announce
     *
     * @return string
     */
    public function getAnnounce()
    {
        return $this->announce;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Section
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set metaTitle
     *
     * @param string $metaTitle
     *
     * @return Section
     */
    public function setMetaTitle($metaTitle)
    {
        $this->meta_title = $metaTitle;

        return $this;
    }

    /**
     * Get metaTitle
     *
     * @return string
     */
    public function getMetaTitle()
    {
        return $this->meta_title;
    }

    /**
     * Set metaDesc
     *
     * @param string $metaDesc
     *
     * @return Section
     */
    public function setMetaDesc($metaDesc)
    {
        $this->meta_desc = $metaDesc;

        return $this;
    }

    /**
     * Get metaDesc
     *
     * @return string
     */
    public function getMetaDesc()
    {
        return $this->meta_desc;
    }

    /**
     * Set metaKey
     *
     * @param string $metaKey
     *
     * @return Section
     */
    public function setMetaKey($metaKey)
    {
        $this->meta_key = $metaKey;

        return $this;
    }

    /**
     * Get metaKey
     *
     * @return string
     */
    public function getMetaKey()
    {
        return $this->meta_key;
    }

    /**
     * Set showTit
     *
     * @param boolean $showTit
     *
     * @return Section
     */
    public function setShowTit($showTit)
    {
        $this->show_tit = $showTit;

        return $this;
    }

    /**
     * Get showTit
     *
     * @return boolean
     */
    public function getShowTit()
    {
        return $this->show_tit;
    }

    /**
     * Set showSub
     *
     * @param boolean $showSub
     *
     * @return Section
     */
    public function setShowSub($showSub)
    {
        $this->show_sub = $showSub;

        return $this;
    }

    /**
     * Get showSub
     *
     * @return boolean
     */
    public function getShowSub()
    {
        return $this->show_sub;
    }

    /**
     * Set menu
     *
     * @param \AppBundle\Entity\Menu $menu
     *
     * @return Section
     */
    public function setMenu(\AppBundle\Entity\Menu $menu = null)
    {
        $this->menu = $menu;

        return $this;
    }

    /**
     * Get menu
     *
     * @return \AppBundle\Entity\Menu
     */
    public function getMenu()
    {
        return $this->menu;
    }

    /**
     * Add building
     *
     * @param \AppBundle\Entity\Building $building
     *
     * @return Section
     */
    public function addBuilding(\AppBundle\Entity\Building $building)
    {
        $this->buildings[] = $building;

        return $this;
    }

    /**
     * Remove building
     *
     * @param \AppBundle\Entity\Building $building
     */
    public function removeBuilding(\AppBundle\Entity\Building $building)
    {
        $this->buildings->removeElement($building);
    }

    /**
     * Get buildings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBuildings()
    {
        return $this->buildings;
    }

    /**
     * Add child
     *
     * @param \AppBundle\Entity\Section $child
     *
     * @return Section
     */
    public function addChild(\AppBundle\Entity\Section $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \AppBundle\Entity\Section $child
     */
    public function removeChild(\AppBundle\Entity\Section $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set parent
     *
     * @param \AppBundle\Entity\Section $parent
     *
     * @return Section
     */
    public function setParent(\AppBundle\Entity\Section $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \AppBundle\Entity\Section
     */
    public function getParent()
    {
        return $this->parent;
    }

    public function getAllParents()
    {
        $parents = array();

        $parent = $this;

        while ($parent) {

            if ($parent = $parent->getParent()) {
                $parents[] = $parent;
            }
        }

        if ($parents) {
            array_pop($parents);
        }

        return $parents;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $photos;


    /**
     * Add photo
     *
     * @param \AppBundle\Entity\Photo $photo
     *
     * @return Section
     */
    public function addPhoto(\AppBundle\Entity\Photo $photo)
    {
        $this->photos[] = $photo;

        return $this;
    }

    /**
     * Remove photo
     *
     * @param \AppBundle\Entity\Photo $photo
     */
    public function removePhoto(\AppBundle\Entity\Photo $photo)
    {
        $this->photos->removeElement($photo);
    }

    /**
     * Get photos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return Section
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return Section
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set mapCoords
     *
     * @param string $mapCoords
     *
     * @return Section
     */
    public function setMapCoords($mapCoords)
    {
        $this->map_coords = $mapCoords;

        return $this;
    }

    /**
     * Get mapCoords
     *
     * @return string
     */
    public function getMapCoords()
    {
        return $this->map_coords;
    }

    public function refreshUpdated()
    {
        $this->setUpdated(new \DateTime());
    }

    public function lifecycleRootProtect()
    {
        //protect from self-parents
        if ($this->getParent() && $this->getParent()->getId() == $this->getId()) {

            throw new \Exception('Parent can not be parent to himself');
        }

        //protect for delroot
        if (mb_strtolower($this->getUrl()) == 'root' && !$this->getParent()) {

            throw new \Exception('Root can not be change or deleted');
        }
    }
}
