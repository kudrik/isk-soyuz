<?php

namespace AppBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class OrderFlatForm extends OrderForm
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $subject = $builder->getData();

        $sameFlats = $subject->getSameFlats();

        if (count($sameFlats) > 1) {

            $builder->add('flatid', ChoiceType::class, array(
                    'label' => 'form.floor',
                    'choices' => $sameFlats,
                    'attr' => array('class' => 'e-select_small'),
                )
            );
        }

        $builder->add('description', TextareaType::class, array(
            'required' => false,
            'label' => false,
            'attr' => array('readonly' => 'readonly', 'placeholder' => 'form.flat_desc'),
        ));

        parent::buildForm($builder, $options);
    }
}