<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use AppBundle\Entity\Order;

class OrderForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('contact_name', TextType::class, array(
                'required' => false,
                'attr' => array('placeholder' => 'form.name',),
                'label' => false,
            )
        );
        $builder->add('contact_phone', TextType::class,  array(
            'attr' => array('placeholder' => 'form.phone',),
            'label' => false,));
        $builder->add('submit', SubmitType::class, array('label' => 'form.send', 'attr'=>array('class'=>'b-but')));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Order::class,
        ));
    }
}