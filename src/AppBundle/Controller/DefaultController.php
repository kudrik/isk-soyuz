<?php

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Section;

use Symfony\Component\HttpFoundation\File\File;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
        $SectionRepositary = $this->getDoctrine()->getRepository(Section::class);

        if ($Section = $SectionRepositary->findOneByUrl('index')) {

            return $this->render('default/index.html.twig', [
                'Page' => $Section,
            ]);
        }
    }

    public function testAction(Request $request)
    {
        /*
        $manager = $this->getDoctrine()->getManager();

        $photo = new \AppBundle\Entity\Photo();

        $photo->setName('test name');

        $file = new File('/var/www/isk-soyuz.dev/src/AppBundle/DataFixtures/files/section_akva.jpg');


        $photo->setFile($file);

        $manager->persist($photo);

        $manager->flush();
        */

        //echo $this->get('app.slugger')->slugify('Привет как ДЕЛАя ЯЯЯDfsedDSS');

        /*


        $passwordEncoder = $this->container->get('security.password_encoder');

        $johnUser = new \AppBundle\Entity\User();
        $johnUser->setEmail('kudrikkk@yandex.ru');
        $encodedPassword = $passwordEncoder->encodePassword($johnUser, 'kudrik');
        $johnUser->setPassword($encodedPassword);

        $manager->persist($johnUser);

        $manager->flush();
        */


        return new \Symfony\Component\HttpFoundation\Response('dsd');
    }


    public function testSecureAction(Request $request)
    {

        return new \Symfony\Component\HttpFoundation\Response('secure section');
    }

}
