<?php

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Section;
use AppBundle\Entity\Order;
use AppBundle\Form\OrderIpotekaForm;

class PageController extends Controller
{
    public function indexAction(int $pageId)
    {
        $SectionRepositary = $this->getDoctrine()->getRepository(Section::class);

        if ($Section = $SectionRepositary->find($pageId)) {

            if ($Section->getType() == 1) {

                $order = new Order();

                $order->setUserinfo($_SERVER['REMOTE_ADDR']);

                $ipotekaForm = $this->createForm(OrderIpotekaForm::class, $order, array(
                    'action' => $this->generateUrl('ipoteka_form')
                ));

                return $this->render('districts/index.html.twig', [
                    'Page' => $Section,
                    'ipotekaPage' => $SectionRepositary->findOneByUrl('ipoteka'),
                    'ipotekaForm' => $ipotekaForm->createView(),
                ]);
            }

            return $this->render('default/page.html.twig', [
                'Page' => $Section,
            ]);
        }
    }
}
