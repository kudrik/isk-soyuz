<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use AppBundle\Entity\Plan;
use AppBundle\Entity\Flat;

class FlatController extends Controller
{
    public function importAction(Request $request)
    {
        $manager = $this->getDoctrine()->getManager();

        $planRepository = $this->getDoctrine()->getRepository(Plan::class);

        $flatRepository = $this->getDoctrine()->getRepository(Flat::class);

        $file = '/home/kplotkin/Downloads/akvarelny.csv';

        $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder()]);

        $deleteAll = true;

        $unknownRows = array();

        // decoding CSV contents
        foreach ($serializer->decode(file_get_contents($file), 'csv') as $row) {

            $ploshadi = explode('/',str_replace(',','.',trim($row['ploshad'])));


            $plan_attr = array(
                'building' => intval($row['dom']),
                's_total' => floatval($ploshadi[0]),
                'rooms' => intval(trim($row['rooms'])),
            );

            if (isset($ploshadi[1])) {
                $plan_attr['s_living'] = floatval($ploshadi[1]);
            }

            if (isset($ploshadi[2])) {
                $plan_attr['s_kitchen'] = floatval($ploshadi[2]);
            }

            if ($plan = $planRepository->findOneBy($plan_attr)) {

                // удаляю старые записи проверяя что удалил один раз
                if ($deleteAll) {

                    $flatRepository->removeAll();

                    $deleteAll = false;
                }

                //parse floors
                $floors = explode(',', str_replace(' ','',$row['etagy']));
                foreach ($floors as $k=>$floor) {

                    $floor2 = explode('-',$floor);

                    if (intval(trim(@$floor2[1]))>0) {

                        unset($floors[$k]);

                        $floor3 = range($floor2[0],$floor2[1]);

                        $floors = array_merge($floors,$floor3);
                    }
                }

                foreach ($floors as $floor) {

                    $flat = new Flat();

                    $hot =  intval($row['skidka']);

                    //в 6 колонке этаж который надо вынести в топ предложения
                    if (intval($row['spec'])>0 AND intval($row['spec'])==$floor) {
                        $hot = 2;
                    }

                    $flat->setPlan($plan);
                    $flat->setFloor($floor);
                    $flat->setPrice( intval(str_replace(array(',','.',' '), '', $row['price'])));
                    $flat->setHot($hot);

                    $manager->persist($flat);

                    $manager->flush();
                }

            } else {

                $unknownRows[] = $row;
            }
        }

        return new \Symfony\Component\HttpFoundation\Response(print_R($unknownRows,1));
    }

    public function exportAction(Request $request)
    {
        $flats = array();

        $last_building_id = 0;

        foreach ($this->getDoctrine()
                     ->getRepository(Flat::class)
                     ->createQueryBuilder('f')
                     ->innerJoin('f.plan','p')
                     ->innerJoin('p.building','b')
                     ->addOrderBy('b.id', 'asc')
                     ->addOrderBy('p.rooms', 'asc')
                     ->addOrderBy('f.price', 'asc')
                     ->getQuery()
                     ->getResult() as $flat) {

            $k = $flat->getPlan()->getId() . '_' . $flat->getPrice();

            if (!isset($flats[$k])) {
                $flats[$k] = array(
                    'dom' => $flat->getPlan()->getBuilding()->getId(),
                    'rooms' => $flat->getPlan()->getRooms(),
                    'etagy' => array(),
                    'ploshad'=> $flat->getPlan()->getSTotal().'/'.$flat->getPlan()->getSLiving().'/'.$flat->getPlan()->getSkitchen(),
                    'price' => $flat->getPrice(),
                    'skidka' => '',
                    'spec' => '',
                    'comment' => '',
                );
            }

            $flats[$k]['etagy'][$flat->getFloor()] = $flat->getFloor();

            if ($flat->getHot()) {
                $flats[$k]['skidka'] = 'Yes';
            }

            if ($flat->getHot() > 1) {
                $flats[$k]['spec'] = $flat->getFloor();
            }

            if ($last_building_id <> $flat->getPlan()->getBuilding()->getId()) {
                $flats[$k]['comment'] = $flat->getPlan()->getBuilding()->getSection()->getName() . ', ' . $flat->getPlan()->getBuilding()->getName() . ', ' . $flat->getPlan()->getBuilding()->getAddress();
            }

            $last_building_id = $flat->getPlan()->getBuilding()->getId();
        }

        $response = $this->render('flat/export.csv.twig', array('flats' => $flats));

        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Disposition', 'attachment; filename="'.$request->server->get('HTTP_HOST').'_'.date('d.m.Y').'.csv"');

        return $response;
    }
}
