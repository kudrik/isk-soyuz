<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\OrderForm;
use AppBundle\Form\OrderExcursionForm;
use AppBundle\Form\OrderTradeinForm;
use AppBundle\Form\OrderIpotekaForm;
use AppBundle\Form\OrderFlatForm;
use AppBundle\Entity\Order;
use AppBundle\Entity\Flat;
use AppBundle\Entity\Section;

class OrderController extends Controller
{
    public function callmeAction(Request $request)
    {
        $order = new Order();
        $order->setTheme('Обратный звонок');
        $order->setUserinfo($_SERVER['REMOTE_ADDR']);

        $form = $this->createForm(OrderForm::class, $order, array(
            'action' => $this->generateUrl($request->get('_route'))
        ));

        $form->handleRequest($request);

        if ($return = $this->_formSubmit($form, $order)) {
            return $return;
        }

        return $this->render('order/callme.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function excursionAction(Request $request)
    {
        $order = new Order();
        $order->setTheme('Экскурсия');
        $order->setUserinfo($_SERVER['REMOTE_ADDR']);

        $form = $this->createForm(
            OrderExcursionForm::class, $order,
            array('action' => $this->generateUrl($request->get('_route')))
        );

        $form->handleRequest($request);

        if ($return = $this->_formSubmit($form, $order)) {
            return $return;
        }

        return $this->render('order/excursion.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function tradeinAction(Request $request)
    {
        $SectionRepositary = $this->getDoctrine()->getRepository(Section::class);

        if ($Section = $SectionRepositary->findOneByUrl('tradein')) {

            $order = new Order();
            $order->setTheme($Section->getName());
            $order->setUserinfo($_SERVER['REMOTE_ADDR']);

            $form = $this->createForm(OrderTradeinForm::class, $order, array(
                'action' => $this->generateUrl($request->get('_route'))
            ));

            $form->handleRequest($request);

            if ($return = $this->_formSubmit($form, $order)) {
                return $return;
            }

            return $this->render('order/tradein.html.twig', [
                'Page' => $Section,
                'form' => $form->createView(),
            ]);
        }
    }

    public function ipotekaAction(Request $request)
    {
        $SectionRepositary = $this->getDoctrine()->getRepository(Section::class);

        if ($Section = $SectionRepositary->findOneByUrl('ipoteka')) {

            $order = new Order();

            $order->setUserinfo($_SERVER['REMOTE_ADDR']);

            $form = $this->createForm(OrderIpotekaForm::class, $order, array(
                'action' => $this->generateUrl($request->get('_route'))
            ));

            $form->handleRequest($request);

            if ($return = $this->_formSubmit($form, $order)) {
                return $return;
            }

            return $this->render('order/tradein.html.twig', [
                'Page' => $Section,
                'form' => $form->createView(),
            ]);
        }

    }

    public function flatAction(int $flat_id)
    {
        $request = $this->get('request_stack')->getCurrentRequest();

        $order = new Order();
        $order->setTheme('Бронирование квартиры');
        $order->setUserinfo($_SERVER['REMOTE_ADDR']);

        if ($flat = $this->getDoctrine()->getRepository(Flat::class)->find($flat_id)) {

            $sameFlats = array();
            //get same flats (price+plan)
            foreach ($this->getDoctrine()->getRepository(Flat::class)->findBy(
                array(
                    'plan'=>$flat->getPlan(),
                    'price'=>$flat->getPrice()
                )
            ) as $sameFlat) {

                $sameFlats[$sameFlat->getFloor()] = $sameFlat->getId();
            }

            $order->setSameFlats($sameFlats);

            $order->setFlatid($flat_id);

            $order->setDescription($flat->getDescription());
        }

        $form = $this->createForm(OrderFlatForm::class, $order, array(
            'action' => $this->generateUrl($request->get('_route'),array('flat_id'=>$flat_id))
        ));

        $form->handleRequest($request);

        //rewrite descrioption of flat after fom submitting
        if ($flat_id<>$order->getFlatid() && $flat = $this->getDoctrine()->getRepository(Flat::class)->find($order->getFlatid())) {

            $order->setDescription($flat->getDescription());
        }

        if ($return = $this->_formSubmit($form, $order)) {
            return $return;
        }

        return $this->render('order/flat.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    protected function _formSubmit($form, $order)
    {
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($order);
            $em->flush();

            return $this->render('order/done.html.twig');
        }
    }
}
